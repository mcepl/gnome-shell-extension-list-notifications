// Simple gnome-shell extension to provide access to the notifications.
//
//   Copyright (C) 2014  Red Hat Corp.
// 
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
// 
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
// 
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.


const St = imports.gi.St;
const Lang = imports.lang;
const Gio = imports.gi.Gio;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

const LN_SERVICE_NAME = 'com.redhat.Notifications';
const LN_SERVICE_PATH = '/com/redhat/Notifications';

const NotificationsIface = '<node>' +
'<interface name="' + LN_SERVICE_NAME + '">' +
'<method name="GotMessage">' +
'    <arg type="s" name="arg" direction="in" />' +
'    <arg type="as" direction="out" />' +
'</method>' +
'</interface>' +
'</node>';

let text, button;

const DigNotificationsDBus = new Lang.Class({
    Name: 'DigNotificationsDBus',

      _init: function() {
        this._dbusImpl = Gio.DBusExportedObject.
            wrapJSObject(NotificationsIface, this);
        this._dbusImpl.export(Gio.DBus.session, LN_SERVICE_PATH);              
      },

      _getNotifications: function() {
        let sources = Main.messageTray.getSources();
        return sources.filter(function (x) { return x.isChat; });
      },

      GotMessage: function(address) {
        return this._getNotifications().map(function (x) {
          return x.title;
        });
      }

});


function init() {
    let DN = new DigNotificationsDBus();
}

function enable() {
    // 
}

function disable() {
    //
}

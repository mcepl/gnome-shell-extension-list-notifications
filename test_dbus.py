#!/usr/bin/python
import logging
logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s',
                    level=logging.DEBUG)

from gi.repository import Gio, GLib

# gdbus call --session --dest org.gnome.Shell \
#        --object-path /com/redhat/Notifications \
#        --method com.redhat.Notifications.GotMessage blabl

bus = Gio.bus_get_sync(Gio.BusType.SESSION, None)
req = bus.call_sync('org.gnome.Shell', '/com/redhat/Notifications',
                    'com.redhat.Notifications', 'GotMessage',
                    GLib.Variant('(s)', ('aaaa',)), None,
                    Gio.DBusProxyFlags.NONE, -1, None)

print(req.unpack()[0])
